class PostImagesController < ApplicationController
  before_action :set_post_image, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @post_images = PostImage.where(user_id: current_user.id).page(params[:page]).per(Constants.POST_IMAGE_LIMIT)
    respond_with(@post_images)
  end

  def show
    return redirect_to root_path, alert: 'Not found' unless login_user?
    respond_with(@post_image)
  end

  def new
    @post_image = PostImage.new
    respond_with(@post_image)
  end

  def edit
    if @post_image.user_id != current_user.id
      redirect_to root_path, alert: 'Not found'
      return
    end
  end

  def create
    @post_image = PostImage.new(post_image_params)
    @post_image.save
    respond_with(@post_image)
  end

  def update
    @post_image.update(post_image_params)
    respond_with(@post_image)
  end

  def destroy
    @post_image.destroy
    respond_with(@post_image)
  end

  private
    def set_post_image
      @post_image = PostImage.find(params[:id])
    end

    def post_image_params
      params.require(:post_image).permit(:name, :user_id, :description, :open_date, :close_date, :limit, :visible, :comment_able, :deleted_at)
    end

    def login_user?
      @post_image.user_id == current_user.id
    end
end
