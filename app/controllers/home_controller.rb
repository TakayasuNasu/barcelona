class HomeController < ApplicationController
  before_action :authenticate_user!
  def index
    @post_images = PostImage
        .where(visible: true)
        .where('open_date <= ?', Time.current)
        .where('close_date >= ?', Time.current)
        .page(params[:page])
        .per(Constants.POST_LIST_LIMIT)

    @comments    = Comment.all
  end
end
