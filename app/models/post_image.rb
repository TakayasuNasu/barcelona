class PostImage < ActiveRecord::Base

  has_many :comments

  mount_uploader :name, PostImageUploader
end
