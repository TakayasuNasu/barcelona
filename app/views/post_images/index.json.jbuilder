json.array!(@post_images) do |post_image|
  json.extract! post_image, :id, :name, :user_id, :open_date, :close_date, :limit, :visible, :comment_able, :deleted_at
  json.url post_image_url(post_image, format: :json)
end
