class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id,  null: false
      t.integer :post_image_id,  null: false
      t.string :name
      t.text :description,  null: false
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :comments, :deleted_at
  end
end
