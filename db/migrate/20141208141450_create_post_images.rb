class CreatePostImages < ActiveRecord::Migration
  def change
    create_table :post_images do |t|
      t.string :name,  null: false
      t.string :description
      t.integer :user_id,  null: false
      t.datetime :open_date
      t.datetime :close_date
      t.integer :limit, default: 100
      t.boolean :visible, default: false
      t.boolean :comment_able, default: true
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :post_images, :deleted_at
  end
end
